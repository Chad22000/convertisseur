import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { UsersServiceComponent } from './users-service/users-service.component';
import { DataComponent } from './data/data.component';
import { FormsModule } from "@angular/forms";
import { Currency } from "./model/currency";

@NgModule({
  declarations: [
    AppComponent,
    UsersServiceComponent,
    DataComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [UsersServiceComponent, Currency],
  bootstrap: [AppComponent]
})
export class AppModule { }
