import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Component({
  selector: 'app-users-service',
  templateUrl: './users-service.component.html',
  styleUrls: ['./users-service.component.css']
})
export class UsersServiceComponent implements OnInit {

  key!: '2dc35e10-83fd-11ec-9743-65e1ba858ee6';

  constructor(private http:HttpClient) { }

  getcomments(): Observable<any> {
    let url = 'https://freecurrencyapi.net/api/v2/latest?apikey=2dc35e10-83fd-11ec-9743-65e1ba858ee6';
    return this.http.get(url)
  }

  ngOnInit(): void {
  }

}
