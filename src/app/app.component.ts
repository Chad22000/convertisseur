import { Component } from '@angular/core';
import {UsersServiceComponent} from "./users-service/users-service.component";
import { DataComponent } from "./data/data.component";
import { Currency } from "./model/currency";
import {CURRENCIES} from "./model/CURRENCIES";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'convertisseur';
  value!: number;
  output!: number;

  constructor(private user: UsersServiceComponent,
              private blabla: Currency) {
    }

  lstComments!: DataComponent[];
  lstCurrency!: Currency[]

/*
  ngOnInit(){
    this.lstCurrency = CURRENCIES;
    this.user.getcomments().subscribe(
      {
        next: data => {
          this.lstComments = data["data"];
        },
        error: err => console.log(err),
        complete: () => {
          for(let key in this.lstComments){
            this.blabla.label = key;
            this.blabla.value = this.lstComments[key];
            console.log(this.blabla.label+"  "+this.blabla.value);
            this.lstCurrency.push(this.blabla);
          }
        }
      });
    console.log(this.lstCurrency);
  }

*/
  ngOnInit(){
    this.lstCurrency = CURRENCIES;
    this.user.getcomments().subscribe( data => {
      for(const [key,val] of Object.entries(data["data"])){
        this.blabla.label = key;
        this.blabla.value = val;
        this.lstCurrency.push(this.blabla);
      }
      console.log(this.lstCurrency);
    });
  }

  onSubmit() {
    return this.value;
  }

  convert() {
    this.output = this.value;
  }

}
