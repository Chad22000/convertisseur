import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  request!: any;
  data!: any;

  constructor() { }

  ngOnInit(): void {
  }

}
